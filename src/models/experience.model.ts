import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';

interface ExperienceAttributes {
  id: number;
  userId: number;
  companyName: string;
  role: string;
  startDate: Date;
  endDate?: Date;
  description?: string;
}

export class Experience
  extends Model<ExperienceAttributes, Optional<ExperienceAttributes, 'id'>>
  implements ExperienceAttributes
{
  id: number;

  userId: number;

  companyName: string;

  role: string;

  startDate: Date;

  endDate?: Date;

  description?: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    Experience.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          field: 'user_id',
          type: DataTypes.INTEGER.UNSIGNED,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id',
          },
        },
        companyName: {
          field: 'company_name',
          type: DataTypes.STRING(256),
          allowNull: false,
        },
        role: {
          type: DataTypes.STRING(256),
          allowNull: false,
        },
        startDate: {
          field: 'startDate',
          type: DataTypes.DATE,
          allowNull: false,
        },
        endDate: {
          field: 'endDate',
          type: DataTypes.DATE,
          allowNull: true,
        },
        description: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
      },
      {
        tableName: 'experiences',
        underscored: true,
        sequelize,
      }
    );
  }

  static associate(models: Models) {
    Experience.belongsTo(models.user, {
      foreignKey: 'user_id',
    });
  }
}
