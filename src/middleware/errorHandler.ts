import { Request, Response, NextFunction } from 'express';
import { logger } from '../libs/logger';

const errorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {
  const correlationId = req.headers['x-request-id'] as string;
  logger.error(`Error occurred with correlation ID ${correlationId}: ${err.message}`);
  res.status(500).json({ message: 'Internal Server Error' });
};

export default errorHandler;
