import { RequestHandler, Request, Response, NextFunction } from 'express';
import passport from 'passport';
import { User } from '../models/user.model';

export const isAuthenticated: RequestHandler = passport.authenticate('jwt', { session: false });

export const isAdmin: RequestHandler = (req: Request, res: Response, next: NextFunction) => {
  const user = req.user as User;
  if (user && user.role === 'Admin') {
    return next();
  }
  res.status(403).json({ message: 'Forbidden' });
};

export const isOwnerOrAdmin =
  (Model: any): RequestHandler =>
  async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;
    const instance = await Model.findByPk(id);

    const user = req.user as User;
    if (user && (user.role === 'Admin' || (instance && instance.userId === user.id))) {
      return next();
    }

    res.status(403).json({ message: 'Forbidden' });
  };
