import { Request, Response, NextFunction } from 'express';

type UserRole = 'Admin' | 'User';

export const roles =
  (allowedRoles: UserRole[]) => (req: Request, res: Response, next: NextFunction) => {
    // @ts-ignore
    const userRole = req.user?.role;
    if (!userRole || !allowedRoles.includes(userRole)) {
      return res.status(403).json({ message: 'Forbidden' });
    }
    next();
  };
