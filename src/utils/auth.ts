import jwt from 'jsonwebtoken';
import { User } from '../models/user.model';

const jwtSecret = 'my_jwt_secret';

export const generateToken = (user: User): string => {
  const payload = {
    id: user.id,
    email: user.email,
    role: user.role,
  };

  return jwt.sign(payload, jwtSecret, { expiresIn: '1d' });
};
