import requestId from 'express-request-id';
import express from 'express';
import path from 'path';
import { Loader } from '../interfaces/general';
import { httpLogger } from '../libs/logger';
import errorHandler from '../middleware/errorHandler';

export const loadMiddlewares: Loader = (app, context) => {
  app.use(requestId());
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(httpLogger);
  app.use('/public', express.static(path.join(__dirname, '../../public')));
  app.use(errorHandler);
};
