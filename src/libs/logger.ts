import pino from 'pino';
import pinoHttp, { stdSerializers } from 'pino-http';

export const logger = pino({});

// @ts-ignore
export const httpLogger = pinoHttp({
  logger,
  customLogLevel: (res, err) => {
    if (res.statusCode >= 400 && res.statusCode < 500) {
      return 'warn';
    } else if (res.statusCode >= 500 || err) {
      return 'error';
    }
    return 'info';
  },
  customAttributeKeys: {
    req: 'request',
    res: 'response',
    err: 'error',
    responseTime: 'timeTaken',
    correlationId: 'correlationId', 
  },
  serializers: {
    req: stdSerializers.req, 
    res: stdSerializers.res, 
    err: stdSerializers.err, 
  },
  autoLogging: true,
});
