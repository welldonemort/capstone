import bcrypt from 'bcrypt';
import fs from 'fs';
import path from 'path';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { User } from '../models/user.model';

const SALT_ROUNDS = 10;
const PROJECT_ROOT = path.resolve(__dirname, '../../');

function isValidEmail(email: string): boolean {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailRegex.test(email);
}

export class AuthService {
  async findUserByEmail(email: string): Promise<User | null> {
    return User.findOne({ where: { email } });
  }

  async registerUser(
    userData: Partial<User>,
    avatarFile: Express.Multer.File
  ): Promise<{
    id?: number;
    firstName?: string;
    lastName?: string;
    title?: string;
    summary?: string;
    email?: string;
    image?: string;
  }> {
    try {
      const { firstName, lastName, title, summary, email, password } = userData;

      if (!firstName || !lastName || !title || !summary || !email || !password) {
        throw new Error('All fields are required');
      }

      if (!isValidEmail(email)) {
        throw new Error('Invalid email format');
      }

      const uniqueFilename = `${uuidv4()}-${avatarFile.originalname}`;
      const avatarPath = path.join(PROJECT_ROOT, 'public', uniqueFilename);
      fs.renameSync(avatarFile.path, avatarPath);

      const hashedPassword = await bcrypt.hash(password, SALT_ROUNDS);

      const newUser = await User.create({
        firstName,
        lastName,
        title,
        summary,
        role: 'User',
        email,
        password: hashedPassword,
        image: uniqueFilename,
      });

      const responseData = _.omit(newUser.toJSON(), ['password', 'createdAt', 'updatedAt']);
      return responseData;
    } catch (error) {
      console.error('Error while registering user:', error.message);
      throw new Error('Failed to register user');
    }
  }
}
