import Redis, { RedisClient, ClientOpts } from 'redis';
import { promisify } from 'util';

export class CacheService {
  private client: RedisClient;
  private getAsync: (key: string) => Promise<string | null>;
  private setAsync: (key: string, value: string, mode?: string, duration?: number) => Promise<unknown>;
  private delAsync: (key: string) => Promise<unknown>;

  constructor() {
    const options: ClientOpts = {
      host: 'localhost',
      port: 6379,
    };

    this.client = Redis.createClient(options);

    this.getAsync = promisify(this.client.get).bind(this.client);
    this.setAsync = promisify(this.client.set).bind(this.client);
    this.delAsync = promisify(this.client.del).bind(this.client);

    this.client.on('error', (err) => {
      console.error('Ошибка соединения с Redis:', err);
    });

    this.client.on('connect', () => {
      console.log('Успешное подключение к Redis');
    });
  }

  async get(key: string): Promise<any> {
    const data: string | null = await this.getAsync(key);
    return data ? JSON.parse(data) : null;
  }

  async set(key: string, value: any, expiresInSeconds?: number): Promise<void> {
    if (expiresInSeconds) {
      await this.setAsync(key, JSON.stringify(value), 'EX', expiresInSeconds);
    } else {
      await this.setAsync(key, JSON.stringify(value));
    }
  }

  async del(key: string): Promise<void> {
    await this.delAsync(key);
  }
}

export const cacheService = new CacheService();
