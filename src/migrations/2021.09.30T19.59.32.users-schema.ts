import { MigrationFn } from 'umzug';
import { DataTypes, Sequelize } from 'sequelize';

export const up: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q.createTable('users', {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    first_name: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    last_name: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    image: {
      type: new DataTypes.STRING(256),
      allowNull: false,
      defaultValue: '',
    },
    title: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    summary: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    role: {
      type: new DataTypes.STRING(50),
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
  });

  await q.bulkInsert('users', [
    {
      first_name: 'Admin',
      last_name: 'Admin',
      image: 'default.png',
      title: 'Administrator',
      summary: 'Admin user with full access',
      role: 'Admin',
      email: 'admin@example.com',
      password: 'adminpassword',
      created_at: new Date(),
      updated_at: new Date(),
    },
  ]);
};

export const down: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q.dropTable('users');
};
