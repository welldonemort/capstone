import request from 'supertest';
import { Experience } from '../models/experience.model';
import { appPromise } from '../index';

let app: any;
beforeAll(async () => {
  app = await appPromise;
});

describe('Experience Router', () => {
  it('should create a new experience', async () => {
    const newExperience = {
      userId: 1,
      companyName: 'Test Company',
      role: 'Developer',
      startDate: '2024-06-01',
      endDate: '2024-06-30',
      description: 'Worked on backend development',
    };

    const response = await request(app)
      .post('/')
      .send(newExperience)
      .expect('Content-Type', /json/)
      .expect(201);

    expect(response.body).toMatchObject(newExperience);

    const createdExperience = await Experience.findByPk(response.body.id);
    expect(createdExperience).toBeDefined();
    expect(createdExperience!.companyName).toBe(newExperience.companyName);
  });

  it('should fetch all experiences', async () => {
    const response = await request(app)
      .get('/')
      .expect('Content-Type', /json/)
      .expect(200);
  
    expect(Array.isArray(response.body)).toBe(true);
  
    expect(response.header['x-total-count']).toBeDefined();
    expect(parseInt(response.header['x-total-count'], 10)).toBeGreaterThan(0);
  });
  
  it('should fetch a single experience by ID', async () => {
    const newExperience = await Experience.create({
      userId: 1,
      companyName: 'Test Company',
      role: 'Developer',
      startDate: new Date('2024-06-01'),
      endDate: new Date('2024-06-30'),
      description: 'Worked on backend development',
    });
  
    const response = await request(app)
      .get(`/${newExperience.id}`)
      .expect('Content-Type', /json/)
      .expect(200);
  
    expect(response.body.id).toBe(newExperience.id);
  });
  
  it('should update an experience by ID', async () => {
    const newExperience = await Experience.create({
      userId: 1,
      companyName: 'Test Company',
      role: 'Developer',
      startDate: new Date('2024-06-01'),
      endDate: new Date('2024-06-30'),
      description: 'Worked on backend development',
    });
  
    const updatedExperience = {
      userId: 1,
      companyName: 'Updated Company',
      role: 'Senior Developer',
      startDate: '2024-07-01',
      endDate: '2024-07-31',
      description: 'Worked on frontend development',
    };
  
    const response = await request(app)
      .put(`/${newExperience.id}`)
      .send(updatedExperience)
      .expect('Content-Type', /json/)
      .expect(200);
  
    expect(response.body).toMatchObject(updatedExperience);
  
    const updatedRecord = await Experience.findByPk(newExperience.id);
    expect(updatedRecord!.companyName).toBe(updatedExperience.companyName);
  });
});

