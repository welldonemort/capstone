import request from 'supertest';
import { Sequelize } from 'sequelize';
import { Project } from '../models/project.model';
import { cacheService } from '../services/cache.service';
import { appPromise } from '../index';

let app: any;

describe('Project Routes', async () => {
  let sequelize: Sequelize;

  app = await appPromise;

  beforeAll(async () => {
    sequelize = new Sequelize({
      dialect: 'sqlite',
      storage: ':memory:',
    });
    Project.defineSchema(sequelize);
    await sequelize.sync({ force: true }); 
  });

  afterAll(async () => {
    await sequelize.close();
  });

  afterEach(async () => {
    await Project.destroy({ where: {} }); 
    await cacheService.del('projects'); 
  });

  it('should create a new project', async () => {
    const newProject = {
      userId: 1,
      description: 'Test project description',
    };

    const response = await request(app).post('/projects').send(newProject);

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
    expect(response.body.userId).toBe(newProject.userId);
    expect(response.body.description).toBe(newProject.description);
  });

  it('should fetch all projects', async () => {
    await Project.bulkCreate([
      { userId: 1, description: 'Project 1' },
      { userId: 1, description: 'Project 2' },
    ]);

    const response = await request(app).get('/projects');

    expect(response.status).toBe(200);
    expect(response.body.length).toBe(2);
    expect(response.headers['x-total-count']).toBeDefined();
  });

  it('should fetch a single project by ID', async () => {
    const project = await Project.create({ userId: 1, description: 'Single project' });

    const response = await request(app).get(`/projects/${project.id}`);

    expect(response.status).toBe(200);
    expect(response.body.id).toBe(project.id);
    expect(response.body.userId).toBe(project.userId);
    expect(response.body.description).toBe(project.description);
  });

  it('should update a project', async () => {
    const project = await Project.create({ userId: 1, description: 'Old description' });

    const updatedProject = {
      userId: 2,
      description: 'Updated description',
    };

    const response = await request(app).put(`/projects/${project.id}`).send(updatedProject);

    expect(response.status).toBe(200);
    expect(response.body.id).toBe(project.id);
    expect(response.body.userId).toBe(updatedProject.userId);
    expect(response.body.description).toBe(updatedProject.description);
  });

  it('should delete a project', async () => {
    const project = await Project.create({ userId: 1, description: 'To be deleted' });

    const response = await request(app).delete(`/projects/${project.id}`);

    expect(response.status).toBe(204);

    const deletedProject = await Project.findByPk(project.id);
    expect(deletedProject).toBeNull();
  });
});
