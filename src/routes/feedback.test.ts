import request from 'supertest';
import { Feedback } from '../models/feedback.model';
import { HttpStatus } from '../constants';
import { appPromise } from '../index';

let app: any;
beforeAll(async () => {
  app = await appPromise;
});

describe('Feedback Router', () => {
  afterEach(async () => {
    await Feedback.destroy({ where: {} });
  });

  afterAll(async () => {
    await Feedback.sequelize?.close();
  });

  it('POST /feedbacks should create a new feedback', async () => {
    const newFeedback = {
      fromUser: 1,
      companyName: 'Test Company',
      toUser: 2,
      content: 'Great work!',
    };

    const response = await request(app)
      .post('/feedbacks')
      .send(newFeedback)
      .expect('Content-Type', /json/)
      .expect(HttpStatus.CREATED);

    expect(response.body).toMatchObject(newFeedback);

    const createdFeedback = await Feedback.findByPk(response.body.id);
    expect(createdFeedback).toBeDefined();
    expect(createdFeedback!.companyName).toBe(newFeedback.companyName);
  });

  it('POST /feedbacks should return 400 if required fields are missing', async () => {
    const invalidFeedback = {
      companyName: 'Test Company',
      toUser: 2,
      content: 'Great work!',
    };

    const response = await request(app)
      .post('/feedbacks')
      .send(invalidFeedback)
      .expect('Content-Type', /json/)
      .expect(HttpStatus.BAD_REQUEST);

    expect(response.body.errors).toBeDefined();
    expect(response.body.errors).toHaveLength(1);
    expect(response.body.errors[0].param).toBe('fromUser');
  });

  it('GET /feedbacks should fetch all feedbacks', async () => {
    await Feedback.create({
      fromUser: 1,
      companyName: 'Test Company 1',
      toUser: 2,
      content: 'Great work!',
    });

    await Feedback.create({
      fromUser: 3,
      companyName: 'Test Company 2',
      toUser: 4,
      content: 'Excellent service!',
    });

    const response = await request(app)
      .get('/feedbacks')
      .expect('Content-Type', /json/)
      .expect(HttpStatus.OK);

    expect(Array.isArray(response.body)).toBe(true);
    expect(response.body).toHaveLength(2);

    expect(response.header['x-total-count']).toBeDefined();
    expect(parseInt(response.header['x-total-count'], 10)).toBe(2);
  });

  it('GET /feedbacks/:id should fetch a single feedback by ID', async () => {
    const newFeedback = await Feedback.create({
      fromUser: 1,
      companyName: 'Test Company',
      toUser: 2,
      content: 'Great work!',
    });

    const response = await request(app)
      .get(`/feedbacks/${newFeedback.id}`)
      .expect('Content-Type', /json/)
      .expect(HttpStatus.OK);

    expect(response.body.id).toBe(newFeedback.id);
  });

  it('GET /feedbacks/:id should return 404 if feedback is not found', async () => {
    const invalidId = 999;

    const response = await request(app)
      .get(`/feedbacks/${invalidId}`)
      .expect('Content-Type', /json/)
      .expect(HttpStatus.NOT_FOUND);

    expect(response.body.message).toBe('Feedback not found');
  });

  it('PUT /feedbacks/:id should update a feedback by ID', async () => {
    const newFeedback = await Feedback.create({
      fromUser: 1,
      companyName: 'Test Company',
      toUser: 2,
      content: 'Great work!',
    });

    const updatedFeedback = {
      fromUser: 1,
      companyName: 'Updated Company',
      toUser: 2,
      content: 'Updated feedback content',
    };

    const response = await request(app)
      .put(`/feedbacks/${newFeedback.id}`)
      .send(updatedFeedback)
      .expect('Content-Type', /json/)
      .expect(HttpStatus.OK);

    expect(response.body).toMatchObject(updatedFeedback);

    const updatedRecord = await Feedback.findByPk(newFeedback.id);
    expect(updatedRecord!.companyName).toBe(updatedFeedback.companyName);
  });

  it('PUT /feedbacks/:id should return 404 if feedback is not found', async () => {
    const invalidId = 999;

    const response = await request(app)
      .put(`/feedbacks/${invalidId}`)
      .send({
        fromUser: 1,
        companyName: 'Updated Company',
        toUser: 2,
        content: 'Updated feedback content',
      })
      .expect('Content-Type', /json/)
      .expect(HttpStatus.NOT_FOUND);

    expect(response.body.message).toBe('Feedback not found');
  });

  it('DELETE /feedbacks/:id should delete a feedback by ID', async () => {
    const newFeedback = await Feedback.create({
      fromUser: 1,
      companyName: 'Test Company',
      toUser: 2,
      content: 'Great work!',
    });

    await request(app)
      .delete(`/feedbacks/${newFeedback.id}`)
      .expect(HttpStatus.NO_CONTENT);

    const deletedFeedback = await Feedback.findByPk(newFeedback.id);
    expect(deletedFeedback).toBeNull();
  });

  it('DELETE /feedbacks/:id should return 404 if feedback is not found', async () => {
    const invalidId = 999;

    const response = await request(app)
      .delete(`/feedbacks/${invalidId}`)
      .expect('Content-Type', /json/)
      .expect(HttpStatus.NOT_FOUND);

    expect(response.body.message).toBe('Feedback not found');
  });
});