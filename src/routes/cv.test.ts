import request from 'supertest';
import { User } from '../models/user.model';
import { Experience } from '../models/experience.model';
import { CacheService } from '../services/cache.service';
import { appPromise } from '../index';

let app: any;
const cacheService = new CacheService();
beforeAll(async () => {
  app = await appPromise;
});

afterEach(async () => {
  await cacheService.del('cv_1');
});

describe('CV Router', () => {
  it('should return CV from cache if available', async () => {
    const cachedCv = {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      title: 'Developer',
      experiences: [] as string[],
      projects: [] as string[],
      feedbacks: [] as string[],
    };
    await cacheService.set('cv_1', cachedCv);

    const response = await request(app).get('/api/user/1/cv');

    expect(response.status).toBe(200);
    expect(response.body).toEqual(cachedCv);
  });

  it('should fetch CV from database if not in cache', async () => {
    const user = await User.create({
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      title: 'Developer',
      email: 'john.doe@example.com',
    });

    await Experience.bulkCreate([
      {
        userId: 1,
        companyName: 'Example Inc.',
        role: 'Software Engineer',
        startDate: new Date('2023-01-01'),
        endDate: new Date('2023-12-31'),
        description: 'Worked on various projects.',
      },
    ]);

    const response = await request(app).get('/api/user/1/cv');

    expect(response.status).toBe(200);
    expect(response.body.id).toBe(user.id);
    expect(response.body.firstName).toBe(user.firstName);
    expect(response.body.experiences.length).toBe(1);
    expect(response.body.projects.length).toBe(0);
    expect(response.body.feedbacks.length).toBe(0);
  });

  it('should return 404 if user is not found', async () => {
    const response = await request(app).get('/api/user/999/cv');

    expect(response.status).toBe(404);
    expect(response.body).toEqual({ message: 'User not found' });
  });

  it('should handle internal server error', async () => {
    jest.spyOn(User, 'findByPk').mockImplementationOnce(() => {
      throw new Error('Database error');
    });

    const response = await request(app).get('/api/user/1/cv');

    expect(response.status).toBe(500);
    expect(response.body).toEqual({ message: 'Internal Server Error' });
  });
});
