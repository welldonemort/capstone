import express, { Request, Response } from 'express';
import { User } from '../models/user.model';
import { Experience } from '../models/experience.model';
import { Project } from '../models/project.model';
import { Feedback } from '../models/feedback.model';
import { Context } from '../interfaces/general';
import { HttpStatus } from '../constants';
import { CacheService } from '../services/cache.service';

const cacheService = new CacheService();

export const makeCvRouter = (context?: Context) => {
  const router = express.Router();

  router.get('/user/:userId/cv', async (req: Request, res: Response) => {
    const { userId } = req.params;
    const cacheKey = `cv_${userId}`;

    try {
      let cv = await cacheService.get(cacheKey);

      if (!cv) {
        const user = await User.findByPk(userId, {
          attributes: ['id', 'firstName', 'lastName', 'title', 'image', 'summary', 'email'],
        });

        if (!user) {
          return res.status(HttpStatus.NOT_FOUND).json({ message: 'User not found' });
        }

        const experiences = await Experience.findAll({
          where: { userId },
          attributes: ['userId', 'companyName', 'role', 'startDate', 'endDate', 'description'],
        });

        const projects = await Project.findAll({
          where: { userId },
          attributes: ['id', 'userId', 'image', 'description'],
        });

        const feedbacks = await Feedback.findAll({
          where: { toUser: userId },
          attributes: ['id', 'fromUser', 'companyName', 'toUser', 'content'],
        });

        cv = {
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          image: user.image,
          summary: user.summary,
          email: user.email,
          experiences,
          projects,
          feedbacks,
        };

        await cacheService.set(cacheKey, cv, 3600);
      }

      res.status(HttpStatus.OK).json(cv);
    } catch (error) {
      console.error('Error fetching CV:', error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
    }
  });

  return router;
};
