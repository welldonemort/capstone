import express, { Request, Response } from 'express';
import multer from 'multer';
import path from 'path';
import passport from 'passport';
import { Context } from '../interfaces/general';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user.model';
import { generateToken } from '../utils/auth';
import { roles } from '../middleware/roles';
import { body, validationResult } from 'express-validator';
import { HttpStatus } from '../constants';
import { cacheService } from '../services/cache.service';

const PROJECT_ROOT = path.resolve(__dirname, '../../');

export const makeAuthRouter = (context: Context) => {
  const router = express.Router();
  const authService = new AuthService();
  const upload = multer({ dest: path.join(PROJECT_ROOT, 'public') });

  router.post(
    '/register',
    upload.single('image'),
    [
      body('firstName').notEmpty().withMessage('First name is required'),
      body('lastName').notEmpty().withMessage('Last name is required'),
      body('title').notEmpty().withMessage('Title is required'),
      body('summary').notEmpty().withMessage('Summary is required'),
      body('email').isEmail().withMessage('Invalid email format'),
      body('password').notEmpty().withMessage('Password is required'),
    ],
    async (req: Request, res: Response) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
        }

        if (!req.file) {
          return res.status(HttpStatus.BAD_REQUEST).json({ message: 'No file uploaded' });
        }

        const { email } = req.body;

        const existingUser = await User.findOne({ where: { email } });
        if (existingUser) {
          return res.status(HttpStatus.BAD_REQUEST).json({ message: 'Email already in use' });
        }

        const user = await authService.registerUser(req.body, req.file);
        res.status(HttpStatus.CREATED).json(user);

        await cacheService.del('users');
      } catch (error) {
        console.error('Error while registering user:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal server error' });
      }
    }
  );

  router.post('/login', upload.none(), async (req: Request, res: Response, next) => {
    passport.authenticate('local', async (err: Error | null, user: User | false, info: any) => {
      try {
        if (err || !user) {
          return res.status(HttpStatus.BAD_REQUEST).json({ message: 'Invalid email or password' });
        }

        req.login(user, { session: false }, async (error) => {
          if (error) {
            return next(error);
          }
          const token = generateToken(user);
          return res.json({ user, token });
        });
      } catch (error) {
        return next(error);
      }
    })(req, res, next);
  });

  router.get('/example', roles(['Admin']), async (req: Request, res: Response) => {
    res.json({ message: 'This endpoint is accessible only for Admins' });
  });

  return router;
};
