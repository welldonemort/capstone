import express, { Request, Response } from 'express';
import multer from 'multer';
import path from 'path';
import fs from 'fs';
import { Project } from '../models/project.model';
import { isAuthenticated, isAdmin, isOwnerOrAdmin } from '../middleware/auth';
import { Context } from '../interfaces/general';
import { HttpStatus } from '../constants';
import { body, param, validationResult } from 'express-validator';
import { cacheService } from '../services/cache.service';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const uploadPath = path.join(__dirname, '../../public/uploads');
    if (!fs.existsSync(uploadPath)) {
      fs.mkdirSync(uploadPath, { recursive: true });
    }
    cb(null, uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname}`);
  },
});

const upload = multer({ storage });

export const makeProjectRouter = (context: Context) => {
  const router = express.Router();

  router.post(
    '/',
    isAuthenticated,
    upload.single('image'),
    [
      body('userId').notEmpty().withMessage('User ID is required'),
      body('description').notEmpty().withMessage('Description is required'),
    ],
    async (req: Request, res: Response) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
      }

      const { userId, description } = req.body;
      const image = req.file?.path;

      try {
        const project = await Project.create({
          userId,
          image,
          description,
        });

        await cacheService.del('projects');

        res.status(HttpStatus.CREATED).json(project);
      } catch (error) {
        console.error('Error creating project:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  router.get('/', isAuthenticated, isAdmin, async (req: Request, res: Response) => {
    const { pageSize = 10, page = 1 } = req.query;
    const cacheKey = `projects:${page}:${pageSize}`;

    try {
      let projects = await cacheService.get(cacheKey);

      if (!projects) {
        const limit = parseInt(pageSize as string, 10);
        const offset = (parseInt(page as string, 10) - 1) * limit;

        projects = await Project.findAndCountAll({ limit, offset });

        await cacheService.set(cacheKey, projects);
      }

      res.set('X-total-count', projects.count.toString());
      res.status(HttpStatus.OK).json(projects.rows);
    } catch (error) {
      console.error('Error fetching projects:', error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
    }
  });

  router.get('/:id', async (req: Request, res: Response) => {
    const { id } = req.params;

    try {
      const project = await Project.findByPk(id);

      if (!project) {
        return res.status(HttpStatus.NOT_FOUND).json({ message: 'Project not found' });
      }

      res.status(HttpStatus.OK).json(project);
    } catch (error) {
      console.error('Error fetching project:', error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
    }
  });

  router.put(
    '/:id',
    isAuthenticated,
    isOwnerOrAdmin(Project),
    upload.single('image'),
    [
      param('id').isInt().withMessage('Project ID must be an integer'),
      body('userId').notEmpty().withMessage('User ID is required'),
      body('description').notEmpty().withMessage('Description is required'),
    ],
    async (req: Request, res: Response) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
      }

      const { id } = req.params;
      const { userId, description } = req.body;
      const image = req.file?.path;

      try {
        const project = await Project.findByPk(id);

        if (!project) {
          return res.status(HttpStatus.NOT_FOUND).json({ message: 'Project not found' });
        }

        project.userId = userId;
        project.description = description;
        if (image) {
          project.image = image;
        }

        await project.save();

        await cacheService.del('projects');

        res.status(HttpStatus.OK).json(project);
      } catch (error) {
        console.error('Error updating project:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  router.delete(
    '/:id',
    isAuthenticated,
    isOwnerOrAdmin(Project),
    async (req: Request, res: Response) => {
      const { id } = req.params;

      try {
        const project = await Project.findByPk(id);

        if (!project) {
          return res.status(HttpStatus.NOT_FOUND).json({ message: 'Project not found' });
        }

        await project.destroy();

        await cacheService.del('projects');

        res.status(HttpStatus.NO_CONTENT).send();
      } catch (error) {
        console.error('Error deleting project:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  return router;
};
