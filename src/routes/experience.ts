import express, { Request, Response } from 'express';
import { Experience } from '../models/experience.model';
import { isAuthenticated, isAdmin, isOwnerOrAdmin } from '../middleware/auth';
import { Context } from '../interfaces/general';
import { HttpStatus } from '../constants';
import { body, param, validationResult } from 'express-validator';
import { cacheService } from '../services/cache.service';

export const makeExperienceRouter = (context: Context) => {
  const router = express.Router();

  router.post(
    '/',
    isAuthenticated,
    [
      body('userId').isInt().withMessage('User ID must be an integer'),
      body('companyName').notEmpty().withMessage('Company name is required'),
      body('role').notEmpty().withMessage('Role is required'),
      body('startDate').isISO8601().withMessage('Invalid date format for start date'),
      body('endDate')
        .optional({ nullable: true })
        .isISO8601()
        .withMessage('Invalid date format for end date'),
      body('description').notEmpty().withMessage('Description is required'),
    ],
    async (req: Request, res: Response) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
      }

      const { userId, companyName, role, startDate, endDate, description } = req.body;

      try {
        const experience = await Experience.create({
          userId,
          companyName,
          role,
          startDate,
          endDate,
          description,
        });

        res.status(HttpStatus.CREATED).json(experience);

        await cacheService.del('experiences');
      } catch (error) {
        console.error('Error creating experience:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  router.get('/', isAuthenticated, isAdmin, async (req: Request, res: Response) => {
    const { pageSize = 10, page = 1 } = req.query;

    try {
      let experiences = await cacheService.get('experiences');

      if (!experiences) {
        const limit = parseInt(pageSize as string, 10);
        const offset = (parseInt(page as string, 10) - 1) * limit;

        experiences = await Experience.findAndCountAll({ limit, offset });

        await cacheService.set('experiences', experiences, 3600);
      }

      res.set('X-total-count', experiences.count.toString());
      res.status(HttpStatus.OK).json(experiences.rows);
    } catch (error) {
      console.error('Error fetching experiences:', error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
    }
  });

  router.get('/:id', async (req: Request, res: Response) => {
    const { id } = req.params;

    try {
      const experience = await Experience.findByPk(id);

      if (!experience) {
        return res.status(HttpStatus.NOT_FOUND).json({ message: 'Experience not found' });
      }

      res.status(HttpStatus.OK).json(experience);
    } catch (error) {
      console.error('Error fetching experience:', error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
    }
  });

  router.put(
    '/:id',
    isAuthenticated,
    isOwnerOrAdmin(Experience),
    [
      param('id').isInt().withMessage('Experience ID must be an integer'),
      body('userId').isInt().withMessage('User ID must be an integer'),
      body('companyName').notEmpty().withMessage('Company name is required'),
      body('role').notEmpty().withMessage('Role is required'),
      body('startDate').isISO8601().withMessage('Invalid date format for start date'),
      body('endDate')
        .optional({ nullable: true })
        .isISO8601()
        .withMessage('Invalid date format for end date'),
      body('description').notEmpty().withMessage('Description is required'),
    ],
    async (req: Request, res: Response) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
      }

      const { id } = req.params;
      const { userId, companyName, role, startDate, endDate, description } = req.body;

      try {
        const experience = await Experience.findByPk(id);

        if (!experience) {
          return res.status(HttpStatus.NOT_FOUND).json({ message: 'Experience not found' });
        }

        experience.userId = userId;
        experience.companyName = companyName;
        experience.role = role;
        experience.startDate = startDate;
        experience.endDate = endDate;
        experience.description = description;

        await experience.save();

        await cacheService.del('experiences');

        res.status(HttpStatus.OK).json(experience);
      } catch (error) {
        console.error('Error updating experience:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  router.delete(
    '/:id',
    isAuthenticated,
    isOwnerOrAdmin(Experience),
    async (req: Request, res: Response) => {
      const { id } = req.params;

      try {
        const experience = await Experience.findByPk(id);

        if (!experience) {
          return res.status(HttpStatus.NOT_FOUND).json({ message: 'Experience not found' });
        }

        await experience.destroy();

        await cacheService.del('experiences');

        res.status(HttpStatus.NO_CONTENT).send();
      } catch (error) {
        console.error('Error deleting experience:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  return router;
};
