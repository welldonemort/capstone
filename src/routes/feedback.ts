import express, { Request, Response } from 'express';
import { Feedback } from '../models/feedback.model';
import { isAuthenticated, isAdmin, isOwnerOrAdmin } from '../middleware/auth';
import { Context } from '../interfaces/general';
import { HttpStatus } from '../constants';
import { body, param, validationResult } from 'express-validator';
import { cacheService } from '../services/cache.service';

export const makeFeedbackRouter = (context: Context) => {
  const router = express.Router();

  router.post(
    '/',
    isAuthenticated,
    [
      body('fromUser').notEmpty().withMessage('From user ID is required'),
      body('companyName').notEmpty().withMessage('Company name is required'),
      body('toUser').notEmpty().withMessage('To user ID is required'),
      body('content').notEmpty().withMessage('Content is required'),
    ],
    async (req: Request, res: Response) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
      }

      const { fromUser, companyName, toUser, content } = req.body;

      try {
        if (fromUser === toUser) {
          return res
            .status(HttpStatus.BAD_REQUEST)
            .json({ message: 'Users cannot leave feedback for themselves' });
        }

        const feedback = await Feedback.create({ fromUser, companyName, toUser, content });

        await cacheService.del('feedbacks');

        res.status(HttpStatus.CREATED).json(feedback);
      } catch (error) {
        console.error('Error creating feedback:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  router.get('/', isAuthenticated, isAdmin, async (req: Request, res: Response) => {
    const { pageSize = 10, page = 1 } = req.query;
    const cacheKey = `feedbacks:${page}:${pageSize}`;

    try {
      let feedbacks = await cacheService.get(cacheKey);

      if (!feedbacks) {
        const limit = parseInt(pageSize as string, 10);
        const offset = (parseInt(page as string, 10) - 1) * limit;

        feedbacks = await Feedback.findAndCountAll({ limit, offset });

        await cacheService.set(cacheKey, feedbacks);
      }

      res.set('X-total-count', feedbacks.count.toString());
      res.status(HttpStatus.OK).json(feedbacks.rows);
    } catch (error) {
      console.error('Error fetching feedbacks:', error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
    }
  });

  router.get('/:id', async (req: Request, res: Response) => {
    const { id } = req.params;

    try {
      const feedback = await Feedback.findByPk(id);

      if (!feedback) {
        return res.status(HttpStatus.NOT_FOUND).json({ message: 'Feedback not found' });
      }

      res.status(HttpStatus.OK).json(feedback);
    } catch (error) {
      console.error('Error fetching feedback:', error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
    }
  });

  router.put(
    '/:id',
    isAuthenticated,
    isOwnerOrAdmin(Feedback),
    [
      param('id').isInt().withMessage('Feedback ID must be an integer'),
      body('fromUser').notEmpty().withMessage('From user ID is required'),
      body('companyName').notEmpty().withMessage('Company name is required'),
      body('toUser').notEmpty().withMessage('To user ID is required'),
      body('content').notEmpty().withMessage('Content is required'),
    ],
    async (req: Request, res: Response) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
      }

      const { id } = req.params;
      const { fromUser, companyName, toUser, content } = req.body;

      try {
        const feedback = await Feedback.findByPk(id);

        if (!feedback) {
          return res.status(HttpStatus.NOT_FOUND).json({ message: 'Feedback not found' });
        }

        feedback.fromUser = fromUser;
        feedback.companyName = companyName;
        feedback.toUser = toUser;
        feedback.content = content;

        await feedback.save();

        await cacheService.del('feedbacks');

        res.status(HttpStatus.OK).json(feedback);
      } catch (error) {
        console.error('Error updating feedback:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  router.delete(
    '/:id',
    isAuthenticated,
    isOwnerOrAdmin(Feedback),
    async (req: Request, res: Response) => {
      const { id } = req.params;

      try {
        const feedback = await Feedback.findByPk(id);

        if (!feedback) {
          return res.status(HttpStatus.NOT_FOUND).json({ message: 'Feedback not found' });
        }

        await feedback.destroy();

        await cacheService.del('feedbacks');

        res.status(HttpStatus.NO_CONTENT).send();
      } catch (error) {
        console.error('Error deleting feedback:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  return router;
};
