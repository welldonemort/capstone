import express, { Request, Response } from 'express';
import bcrypt from 'bcrypt';
import multer from 'multer';
import { Context } from '../interfaces/general';
import { User } from '../models/user.model';
import { HttpStatus } from '../constants';
import { isAuthenticated, isAdmin, isOwnerOrAdmin } from '../middleware/auth';
import { body, param, validationResult } from 'express-validator';
import { cacheService } from '../services/cache.service';

const upload = multer();

export const makeUsersRouter = (context: Context) => {
  const router = express.Router();

  router.post(
    '/',
    isAuthenticated,
    isAdmin,
    upload.none(),
    [
      body('firstName').notEmpty().withMessage('First name is required'),
      body('lastName').notEmpty().withMessage('Last name is required'),
      body('email').isEmail().withMessage('Invalid email'),
      body('password').notEmpty().withMessage('Password is required'),
      body('role').notEmpty().withMessage('Role is required'),
    ],
    async (req: Request, res: Response) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
      }

      const { firstName, lastName, title, summary, email, password, role } = req.body;

      try {
        const hashedPassword = await bcrypt.hash(password, 10);

        const user = await User.create({
          firstName,
          lastName,
          title,
          summary,
          email,
          password: hashedPassword,
          role,
        });

        await cacheService.del('users');

        res.status(HttpStatus.CREATED).json(user);
      } catch (error) {
        console.error('Error creating user:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  router.get('/', isAuthenticated, isAdmin, async (req: Request, res: Response) => {
    try {
      const { pageSize = 10, page = 1 } = req.query;
      const cacheKey = `users:${page}:${pageSize}`;

      let users = await cacheService.get(cacheKey);

      if (!users) {
        const pageSizeNum = Number(pageSize);
        const pageNum = Number(page);

        users = await User.findAll({
          limit: pageSizeNum,
          offset: (pageNum - 1) * pageSizeNum,
        });

        await cacheService.set(cacheKey, users);
      }

      const totalCount = await User.count();

      res.setHeader('X-total-count', totalCount.toString());

      res.status(HttpStatus.OK).json(users);
    } catch (error) {
      console.error('Error fetching users:', error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
    }
  });

  router.get('/:id', async (req: Request, res: Response) => {
    try {
      const { id } = req.params;

      const user = await User.findByPk(id);

      if (!user) {
        return res.status(HttpStatus.NOT_FOUND).json({ message: 'User not found' });
      }

      res.status(HttpStatus.OK).json(user);
    } catch (error) {
      console.error('Error fetching user:', error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
    }
  });

  router.put(
    '/:id',
    isAuthenticated,
    isOwnerOrAdmin(User),
    [
      param('id').isInt().withMessage('User ID must be an integer'),
      body('firstName').notEmpty().withMessage('First name is required'),
      body('lastName').notEmpty().withMessage('Last name is required'),
      body('email').isEmail().withMessage('Invalid email'),
      body('role').notEmpty().withMessage('Role is required'),
    ],
    async (req: Request, res: Response) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
      }

      const { id } = req.params;
      const { firstName, lastName, title, summary, email, password, role } = req.body;

      try {
        const user = await User.findByPk(id);

        if (!user) {
          return res.status(HttpStatus.NOT_FOUND).json({ message: 'User not found' });
        }

        let hashedPassword;
        if (password) {
          hashedPassword = await bcrypt.hash(password, 10);
        }

        await user.update({
          firstName,
          lastName,
          title,
          summary,
          email,
          password: hashedPassword || user.password,
          role,
        });

        await cacheService.del('users');

        res.status(HttpStatus.OK).json(user);
      } catch (error) {
        console.error('Error updating user:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  router.delete(
    '/:id',
    isAuthenticated,
    isOwnerOrAdmin(User),
    async (req: Request, res: Response) => {
      try {
        const { id } = req.params;

        const user = await User.findByPk(id);

        if (!user) {
          return res.status(HttpStatus.NOT_FOUND).json({ message: 'User not found' });
        }

        await user.destroy();
        await cacheService.del('users');

        res.status(HttpStatus.NO_CONTENT).send();
      } catch (error) {
        console.error('Error deleting user:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
      }
    }
  );

  return router;
};
