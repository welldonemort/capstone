import request from 'supertest';
import { Sequelize } from 'sequelize';
import { User } from '../models/user.model';
import { cacheService } from '../services/cache.service';
import { appPromise } from '../index';

let app: any;
beforeAll(async () => {
});

describe('User Routes', async () => {
  let sequelize: Sequelize;

  beforeAll(async () => {
    app = await appPromise;
    sequelize = new Sequelize({
      dialect: 'sqlite',
      storage: ':memory:',
    });
    User.initialize(sequelize);
    await sequelize.sync({ force: true });
  });

  afterAll(async () => {
    await sequelize.close();
  });

  afterEach(async () => {
    await User.destroy({ where: {} });
    await cacheService.del('users');
  });

  it('should create a new user', async () => {
    const newUser = {
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
      password: 'password',
      role: 'user',
    };

    const response = await request(app).post('/users').send(newUser);

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
    expect(response.body.firstName).toBe(newUser.firstName);
    expect(response.body.lastName).toBe(newUser.lastName);
    expect(response.body.email).toBe(newUser.email);
    expect(response.body.role).toBe(newUser.role);
  });

  it('should fetch all users', async () => {
    await User.bulkCreate([
      { firstName: 'John', lastName: 'Doe', email: 'john.doe@example.com', password: 'password', role: 'User' },
      { firstName: 'Jane', lastName: 'Smith', email: 'jane.smith@example.com', password: 'password', role: 'Admin' },
    ]);

    const response = await request(app).get('/users');

    expect(response.status).toBe(200);
    expect(response.body.length).toBe(2);
    expect(response.headers['x-total-count']).toBeDefined();
  });

  it('should fetch a single user by ID', async () => {
    const user = await User.create({
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
      password: 'password',
      role: 'User',
    });

    const response = await request(app).get(`/users/${user.id}`);

    expect(response.status).toBe(200);
    expect(response.body.id).toBe(user.id);
    expect(response.body.firstName).toBe(user.firstName);
    expect(response.body.lastName).toBe(user.lastName);
    expect(response.body.email).toBe(user.email);
    expect(response.body.role).toBe(user.role);
  });

  it('should update a user', async () => {
    const user = await User.create({
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
      password: 'password',
      role: 'User',
    });

    const updatedUser = {
      firstName: 'Updated',
      lastName: 'User',
      email: 'updated.user@example.com',
      role: 'admin',
    };

    const response = await request(app).put(`/users/${user.id}`).send(updatedUser);

    expect(response.status).toBe(200);
    expect(response.body.id).toBe(user.id);
    expect(response.body.firstName).toBe(updatedUser.firstName);
    expect(response.body.lastName).toBe(updatedUser.lastName);
    expect(response.body.email).toBe(updatedUser.email);
    expect(response.body.role).toBe(updatedUser.role);
  });

  it('should delete a user', async () => {
    const user = await User.create({
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
      password: 'password',
      role: 'User',
    });

    const response = await request(app).delete(`/users/${user.id}`);

    expect(response.status).toBe(204);

    const deletedUser = await User.findByPk(user.id);
    expect(deletedUser).toBeNull();
  });
});
