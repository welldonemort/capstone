"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadRoutes = void 0;
const auth_1 = require("../routes/auth");
const users_1 = require("../routes/users");
const experience_1 = require("../routes/experience");
const feedback_1 = require("../routes/feedback");
const projects_1 = require("../routes/projects");
const cv_1 = require("../routes/cv");
const loadRoutes = (app, context) => {
    app.use('/api/auth', (0, auth_1.makeAuthRouter)(context));
    app.use('/api/users', (0, users_1.makeUsersRouter)(context));
    app.use('/api/experience', (0, experience_1.makeExperienceRouter)(context));
    app.use('/api/feedback', (0, feedback_1.makeFeedbackRouter)(context));
    app.use('/api/projects', (0, projects_1.makeProjectRouter)(context));
    app.use('/api', (0, cv_1.makeCvRouter)(context));
};
exports.loadRoutes = loadRoutes;
//# sourceMappingURL=routes.js.map