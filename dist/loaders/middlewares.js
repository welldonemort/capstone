"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadMiddlewares = void 0;
const express_request_id_1 = __importDefault(require("express-request-id"));
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const logger_1 = require("../libs/logger");
const errorHandler_1 = __importDefault(require("../middleware/errorHandler"));
const loadMiddlewares = (app, context) => {
    app.use((0, express_request_id_1.default)());
    app.use(express_1.default.json());
    app.use(express_1.default.urlencoded({ extended: true }));
    app.use(logger_1.httpLogger);
    app.use('/public', express_1.default.static(path_1.default.join(__dirname, '../../public')));
    app.use(errorHandler_1.default);
};
exports.loadMiddlewares = loadMiddlewares;
//# sourceMappingURL=middlewares.js.map