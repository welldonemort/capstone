"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.roles = void 0;
const roles = (allowedRoles) => (req, res, next) => {
    var _a;
    // @ts-ignore
    const userRole = (_a = req.user) === null || _a === void 0 ? void 0 : _a.role;
    if (!userRole || !allowedRoles.includes(userRole)) {
        return res.status(403).json({ message: 'Forbidden' });
    }
    next();
};
exports.roles = roles;
//# sourceMappingURL=roles.js.map