"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isOwnerOrAdmin = exports.isAdmin = exports.isAuthenticated = void 0;
const passport_1 = __importDefault(require("passport"));
exports.isAuthenticated = passport_1.default.authenticate('jwt', { session: false });
const isAdmin = (req, res, next) => {
    const user = req.user;
    if (user && user.role === 'Admin') {
        return next();
    }
    res.status(403).json({ message: 'Forbidden' });
};
exports.isAdmin = isAdmin;
const isOwnerOrAdmin = (Model) => (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const instance = yield Model.findByPk(id);
    const user = req.user;
    if (user && (user.role === 'Admin' || (instance && instance.userId === user.id))) {
        return next();
    }
    res.status(403).json({ message: 'Forbidden' });
});
exports.isOwnerOrAdmin = isOwnerOrAdmin;
//# sourceMappingURL=auth.js.map