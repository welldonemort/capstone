"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("../libs/logger");
const errorHandler = (err, req, res, next) => {
    const correlationId = req.headers['x-request-id'];
    logger_1.logger.error(`Error occurred with correlation ID ${correlationId}: ${err.message}`);
    res.status(500).json({ message: 'Internal Server Error' });
};
exports.default = errorHandler;
//# sourceMappingURL=errorHandler.js.map