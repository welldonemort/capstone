"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const lodash_1 = __importDefault(require("lodash"));
const uuid_1 = require("uuid");
const user_model_1 = require("../models/user.model");
const SALT_ROUNDS = 10;
const PROJECT_ROOT = path_1.default.resolve(__dirname, '../../');
function isValidEmail(email) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
}
class AuthService {
    findUserByEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            return user_model_1.User.findOne({ where: { email } });
        });
    }
    registerUser(userData, avatarFile) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { firstName, lastName, title, summary, email, password } = userData;
                if (!firstName || !lastName || !title || !summary || !email || !password) {
                    throw new Error('All fields are required');
                }
                if (!isValidEmail(email)) {
                    throw new Error('Invalid email format');
                }
                const uniqueFilename = `${(0, uuid_1.v4)()}-${avatarFile.originalname}`;
                const avatarPath = path_1.default.join(PROJECT_ROOT, 'public', uniqueFilename);
                fs_1.default.renameSync(avatarFile.path, avatarPath);
                const hashedPassword = yield bcrypt_1.default.hash(password, SALT_ROUNDS);
                const newUser = yield user_model_1.User.create({
                    firstName,
                    lastName,
                    title,
                    summary,
                    role: 'User',
                    email,
                    password: hashedPassword,
                    image: uniqueFilename,
                });
                const responseData = lodash_1.default.omit(newUser.toJSON(), ['password', 'createdAt', 'updatedAt']);
                return responseData;
            }
            catch (error) {
                console.error('Error while registering user:', error.message);
                throw new Error('Failed to register user');
            }
        });
    }
}
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map