"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cacheService = exports.CacheService = void 0;
const redis_1 = __importDefault(require("redis"));
const util_1 = require("util");
class CacheService {
    constructor() {
        const options = {
            host: 'localhost',
            port: 6379,
        };
        this.client = redis_1.default.createClient(options);
        this.getAsync = (0, util_1.promisify)(this.client.get).bind(this.client);
        this.setAsync = (0, util_1.promisify)(this.client.set).bind(this.client);
        this.delAsync = (0, util_1.promisify)(this.client.del).bind(this.client);
        this.client.on('error', (err) => {
            console.error('Ошибка соединения с Redis:', err);
        });
        this.client.on('connect', () => {
            console.log('Успешное подключение к Redis');
        });
    }
    get(key) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.getAsync(key);
            return data ? JSON.parse(data) : null;
        });
    }
    set(key, value, expiresInSeconds) {
        return __awaiter(this, void 0, void 0, function* () {
            if (expiresInSeconds) {
                yield this.setAsync(key, JSON.stringify(value), 'EX', expiresInSeconds);
            }
            else {
                yield this.setAsync(key, JSON.stringify(value));
            }
        });
    }
    del(key) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.delAsync(key);
        });
    }
}
exports.CacheService = CacheService;
exports.cacheService = new CacheService();
//# sourceMappingURL=cache.service.js.map