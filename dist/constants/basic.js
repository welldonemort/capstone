
// constants.ts
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpStatus = void 0;
exports.HttpStatus = {
    OK: 200,
    CREATED: 201,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    INTERNAL_SERVER_ERROR: 500,
};
// # sourceMappingURL=basic.js.map