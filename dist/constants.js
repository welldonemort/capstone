"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.emailRegex = exports.HttpStatus = void 0;
exports.HttpStatus = {
    OK: 200,
    CREATED: 201,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    NO_CONTENT: 204,
    INTERNAL_SERVER_ERROR: 500,
};
exports.emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
//# sourceMappingURL=constants.js.map