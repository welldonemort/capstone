"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeExperienceRouter = void 0;
const express_1 = __importDefault(require("express"));
const experience_model_1 = require("../models/experience.model");
const auth_1 = require("../middleware/auth");
const constants_1 = require("../constants");
const express_validator_1 = require("express-validator");
const cache_service_1 = require("../services/cache.service");
const makeExperienceRouter = (context) => {
    const router = express_1.default.Router();
    router.post('/', auth_1.isAuthenticated, [
        (0, express_validator_1.body)('userId').isInt().withMessage('User ID must be an integer'),
        (0, express_validator_1.body)('companyName').notEmpty().withMessage('Company name is required'),
        (0, express_validator_1.body)('role').notEmpty().withMessage('Role is required'),
        (0, express_validator_1.body)('startDate').isISO8601().withMessage('Invalid date format for start date'),
        (0, express_validator_1.body)('endDate')
            .optional({ nullable: true })
            .isISO8601()
            .withMessage('Invalid date format for end date'),
        (0, express_validator_1.body)('description').notEmpty().withMessage('Description is required'),
    ], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
        }
        const { userId, companyName, role, startDate, endDate, description } = req.body;
        try {
            const experience = yield experience_model_1.Experience.create({
                userId,
                companyName,
                role,
                startDate,
                endDate,
                description,
            });
            res.status(constants_1.HttpStatus.CREATED).json(experience);
            yield cache_service_1.cacheService.del('experiences');
        }
        catch (error) {
            console.error('Error creating experience:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.get('/', auth_1.isAuthenticated, auth_1.isAdmin, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { pageSize = 10, page = 1 } = req.query;
        try {
            let experiences = yield cache_service_1.cacheService.get('experiences');
            if (!experiences) {
                const limit = parseInt(pageSize, 10);
                const offset = (parseInt(page, 10) - 1) * limit;
                experiences = yield experience_model_1.Experience.findAndCountAll({ limit, offset });
                yield cache_service_1.cacheService.set('experiences', experiences, 3600);
            }
            res.set('X-total-count', experiences.count.toString());
            res.status(constants_1.HttpStatus.OK).json(experiences.rows);
        }
        catch (error) {
            console.error('Error fetching experiences:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { id } = req.params;
        try {
            const experience = yield experience_model_1.Experience.findByPk(id);
            if (!experience) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'Experience not found' });
            }
            res.status(constants_1.HttpStatus.OK).json(experience);
        }
        catch (error) {
            console.error('Error fetching experience:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.put('/:id', auth_1.isAuthenticated, (0, auth_1.isOwnerOrAdmin)(experience_model_1.Experience), [
        (0, express_validator_1.param)('id').isInt().withMessage('Experience ID must be an integer'),
        (0, express_validator_1.body)('userId').isInt().withMessage('User ID must be an integer'),
        (0, express_validator_1.body)('companyName').notEmpty().withMessage('Company name is required'),
        (0, express_validator_1.body)('role').notEmpty().withMessage('Role is required'),
        (0, express_validator_1.body)('startDate').isISO8601().withMessage('Invalid date format for start date'),
        (0, express_validator_1.body)('endDate')
            .optional({ nullable: true })
            .isISO8601()
            .withMessage('Invalid date format for end date'),
        (0, express_validator_1.body)('description').notEmpty().withMessage('Description is required'),
    ], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
        }
        const { id } = req.params;
        const { userId, companyName, role, startDate, endDate, description } = req.body;
        try {
            const experience = yield experience_model_1.Experience.findByPk(id);
            if (!experience) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'Experience not found' });
            }
            experience.userId = userId;
            experience.companyName = companyName;
            experience.role = role;
            experience.startDate = startDate;
            experience.endDate = endDate;
            experience.description = description;
            yield experience.save();
            yield cache_service_1.cacheService.del('experiences');
            res.status(constants_1.HttpStatus.OK).json(experience);
        }
        catch (error) {
            console.error('Error updating experience:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.delete('/:id', auth_1.isAuthenticated, (0, auth_1.isOwnerOrAdmin)(experience_model_1.Experience), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { id } = req.params;
        try {
            const experience = yield experience_model_1.Experience.findByPk(id);
            if (!experience) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'Experience not found' });
            }
            yield experience.destroy();
            yield cache_service_1.cacheService.del('experiences');
            res.status(constants_1.HttpStatus.NO_CONTENT).send();
        }
        catch (error) {
            console.error('Error deleting experience:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    return router;
};
exports.makeExperienceRouter = makeExperienceRouter;
//# sourceMappingURL=experience.js.map