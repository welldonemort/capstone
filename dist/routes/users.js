"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeUsersRouter = void 0;
const express_1 = __importDefault(require("express"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const multer_1 = __importDefault(require("multer"));
const user_model_1 = require("../models/user.model");
const constants_1 = require("../constants");
const auth_1 = require("../middleware/auth");
const express_validator_1 = require("express-validator");
const cache_service_1 = require("../services/cache.service");
const upload = (0, multer_1.default)();
const makeUsersRouter = (context) => {
    const router = express_1.default.Router();
    router.post('/', auth_1.isAuthenticated, auth_1.isAdmin, upload.none(), [
        (0, express_validator_1.body)('firstName').notEmpty().withMessage('First name is required'),
        (0, express_validator_1.body)('lastName').notEmpty().withMessage('Last name is required'),
        (0, express_validator_1.body)('email').isEmail().withMessage('Invalid email'),
        (0, express_validator_1.body)('password').notEmpty().withMessage('Password is required'),
        (0, express_validator_1.body)('role').notEmpty().withMessage('Role is required'),
    ], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
        }
        const { firstName, lastName, title, summary, email, password, role } = req.body;
        try {
            const hashedPassword = yield bcrypt_1.default.hash(password, 10);
            const user = yield user_model_1.User.create({
                firstName,
                lastName,
                title,
                summary,
                email,
                password: hashedPassword,
                role,
            });
            yield cache_service_1.cacheService.del('users');
            res.status(constants_1.HttpStatus.CREATED).json(user);
        }
        catch (error) {
            console.error('Error creating user:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.get('/', auth_1.isAuthenticated, auth_1.isAdmin, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { pageSize = 10, page = 1 } = req.query;
            const cacheKey = `users:${page}:${pageSize}`;
            let users = yield cache_service_1.cacheService.get(cacheKey);
            if (!users) {
                const pageSizeNum = Number(pageSize);
                const pageNum = Number(page);
                users = yield user_model_1.User.findAll({
                    limit: pageSizeNum,
                    offset: (pageNum - 1) * pageSizeNum,
                });
                yield cache_service_1.cacheService.set(cacheKey, users);
            }
            const totalCount = yield user_model_1.User.count();
            res.setHeader('X-total-count', totalCount.toString());
            res.status(constants_1.HttpStatus.OK).json(users);
        }
        catch (error) {
            console.error('Error fetching users:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            const user = yield user_model_1.User.findByPk(id);
            if (!user) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'User not found' });
            }
            res.status(constants_1.HttpStatus.OK).json(user);
        }
        catch (error) {
            console.error('Error fetching user:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.put('/:id', auth_1.isAuthenticated, (0, auth_1.isOwnerOrAdmin)(user_model_1.User), [
        (0, express_validator_1.param)('id').isInt().withMessage('User ID must be an integer'),
        (0, express_validator_1.body)('firstName').notEmpty().withMessage('First name is required'),
        (0, express_validator_1.body)('lastName').notEmpty().withMessage('Last name is required'),
        (0, express_validator_1.body)('email').isEmail().withMessage('Invalid email'),
        (0, express_validator_1.body)('role').notEmpty().withMessage('Role is required'),
    ], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
        }
        const { id } = req.params;
        const { firstName, lastName, title, summary, email, password, role } = req.body;
        try {
            const user = yield user_model_1.User.findByPk(id);
            if (!user) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'User not found' });
            }
            let hashedPassword;
            if (password) {
                hashedPassword = yield bcrypt_1.default.hash(password, 10);
            }
            yield user.update({
                firstName,
                lastName,
                title,
                summary,
                email,
                password: hashedPassword || user.password,
                role,
            });
            yield cache_service_1.cacheService.del('users');
            res.status(constants_1.HttpStatus.OK).json(user);
        }
        catch (error) {
            console.error('Error updating user:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.delete('/:id', auth_1.isAuthenticated, (0, auth_1.isOwnerOrAdmin)(user_model_1.User), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = req.params;
            const user = yield user_model_1.User.findByPk(id);
            if (!user) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'User not found' });
            }
            yield user.destroy();
            yield cache_service_1.cacheService.del('users');
            res.status(constants_1.HttpStatus.NO_CONTENT).send();
        }
        catch (error) {
            console.error('Error deleting user:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    return router;
};
exports.makeUsersRouter = makeUsersRouter;
//# sourceMappingURL=users.js.map