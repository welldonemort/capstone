"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const user_model_1 = require("../models/user.model");
const experience_model_1 = require("../models/experience.model");
const cache_service_1 = require("../services/cache.service");
const index_1 = require("../index");
let app;
const cacheService = new cache_service_1.CacheService();
beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
    app = yield index_1.appPromise;
}));
afterEach(() => __awaiter(void 0, void 0, void 0, function* () {
    yield cacheService.del('cv_1');
}));
describe('CV Router', () => {
    it('should return CV from cache if available', () => __awaiter(void 0, void 0, void 0, function* () {
        const cachedCv = {
            id: 1,
            firstName: 'John',
            lastName: 'Doe',
            title: 'Developer',
            experiences: [],
            projects: [],
            feedbacks: [],
        };
        yield cacheService.set('cv_1', cachedCv);
        const response = yield (0, supertest_1.default)(app).get('/api/user/1/cv');
        expect(response.status).toBe(200);
        expect(response.body).toEqual(cachedCv);
    }));
    it('should fetch CV from database if not in cache', () => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield user_model_1.User.create({
            id: 1,
            firstName: 'John',
            lastName: 'Doe',
            title: 'Developer',
            email: 'john.doe@example.com',
        });
        yield experience_model_1.Experience.bulkCreate([
            {
                userId: 1,
                companyName: 'Example Inc.',
                role: 'Software Engineer',
                startDate: new Date('2023-01-01'),
                endDate: new Date('2023-12-31'),
                description: 'Worked on various projects.',
            },
        ]);
        const response = yield (0, supertest_1.default)(app).get('/api/user/1/cv');
        expect(response.status).toBe(200);
        expect(response.body.id).toBe(user.id);
        expect(response.body.firstName).toBe(user.firstName);
        expect(response.body.experiences.length).toBe(1);
        expect(response.body.projects.length).toBe(0);
        expect(response.body.feedbacks.length).toBe(0);
    }));
    it('should return 404 if user is not found', () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app).get('/api/user/999/cv');
        expect(response.status).toBe(404);
        expect(response.body).toEqual({ message: 'User not found' });
    }));
    it('should handle internal server error', () => __awaiter(void 0, void 0, void 0, function* () {
        jest.spyOn(user_model_1.User, 'findByPk').mockImplementationOnce(() => {
            throw new Error('Database error');
        });
        const response = yield (0, supertest_1.default)(app).get('/api/user/1/cv');
        expect(response.status).toBe(500);
        expect(response.body).toEqual({ message: 'Internal Server Error' });
    }));
});
//# sourceMappingURL=cv.test%20copy.js.map