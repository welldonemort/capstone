"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const feedback_model_1 = require("../models/feedback.model");
const constants_1 = require("../constants");
const index_1 = require("../index");
let app;
beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
    app = yield index_1.appPromise;
}));
describe('Feedback Router', () => {
    afterEach(() => __awaiter(void 0, void 0, void 0, function* () {
        yield feedback_model_1.Feedback.destroy({ where: {} });
    }));
    afterAll(() => __awaiter(void 0, void 0, void 0, function* () {
        var _a;
        yield ((_a = feedback_model_1.Feedback.sequelize) === null || _a === void 0 ? void 0 : _a.close());
    }));
    it('POST /feedbacks should create a new feedback', () => __awaiter(void 0, void 0, void 0, function* () {
        const newFeedback = {
            fromUser: 1,
            companyName: 'Test Company',
            toUser: 2,
            content: 'Great work!',
        };
        const response = yield (0, supertest_1.default)(app)
            .post('/feedbacks')
            .send(newFeedback)
            .expect('Content-Type', /json/)
            .expect(constants_1.HttpStatus.CREATED);
        expect(response.body).toMatchObject(newFeedback);
        const createdFeedback = yield feedback_model_1.Feedback.findByPk(response.body.id);
        expect(createdFeedback).toBeDefined();
        expect(createdFeedback.companyName).toBe(newFeedback.companyName);
    }));
    it('POST /feedbacks should return 400 if required fields are missing', () => __awaiter(void 0, void 0, void 0, function* () {
        const invalidFeedback = {
            companyName: 'Test Company',
            toUser: 2,
            content: 'Great work!',
        };
        const response = yield (0, supertest_1.default)(app)
            .post('/feedbacks')
            .send(invalidFeedback)
            .expect('Content-Type', /json/)
            .expect(constants_1.HttpStatus.BAD_REQUEST);
        expect(response.body.errors).toBeDefined();
        expect(response.body.errors).toHaveLength(1);
        expect(response.body.errors[0].param).toBe('fromUser');
    }));
    it('GET /feedbacks should fetch all feedbacks', () => __awaiter(void 0, void 0, void 0, function* () {
        yield feedback_model_1.Feedback.create({
            fromUser: 1,
            companyName: 'Test Company 1',
            toUser: 2,
            content: 'Great work!',
        });
        yield feedback_model_1.Feedback.create({
            fromUser: 3,
            companyName: 'Test Company 2',
            toUser: 4,
            content: 'Excellent service!',
        });
        const response = yield (0, supertest_1.default)(app)
            .get('/feedbacks')
            .expect('Content-Type', /json/)
            .expect(constants_1.HttpStatus.OK);
        expect(Array.isArray(response.body)).toBe(true);
        expect(response.body).toHaveLength(2);
        expect(response.header['x-total-count']).toBeDefined();
        expect(parseInt(response.header['x-total-count'], 10)).toBe(2);
    }));
    it('GET /feedbacks/:id should fetch a single feedback by ID', () => __awaiter(void 0, void 0, void 0, function* () {
        const newFeedback = yield feedback_model_1.Feedback.create({
            fromUser: 1,
            companyName: 'Test Company',
            toUser: 2,
            content: 'Great work!',
        });
        const response = yield (0, supertest_1.default)(app)
            .get(`/feedbacks/${newFeedback.id}`)
            .expect('Content-Type', /json/)
            .expect(constants_1.HttpStatus.OK);
        expect(response.body.id).toBe(newFeedback.id);
    }));
    it('GET /feedbacks/:id should return 404 if feedback is not found', () => __awaiter(void 0, void 0, void 0, function* () {
        const invalidId = 999;
        const response = yield (0, supertest_1.default)(app)
            .get(`/feedbacks/${invalidId}`)
            .expect('Content-Type', /json/)
            .expect(constants_1.HttpStatus.NOT_FOUND);
        expect(response.body.message).toBe('Feedback not found');
    }));
    it('PUT /feedbacks/:id should update a feedback by ID', () => __awaiter(void 0, void 0, void 0, function* () {
        const newFeedback = yield feedback_model_1.Feedback.create({
            fromUser: 1,
            companyName: 'Test Company',
            toUser: 2,
            content: 'Great work!',
        });
        const updatedFeedback = {
            fromUser: 1,
            companyName: 'Updated Company',
            toUser: 2,
            content: 'Updated feedback content',
        };
        const response = yield (0, supertest_1.default)(app)
            .put(`/feedbacks/${newFeedback.id}`)
            .send(updatedFeedback)
            .expect('Content-Type', /json/)
            .expect(constants_1.HttpStatus.OK);
        expect(response.body).toMatchObject(updatedFeedback);
        const updatedRecord = yield feedback_model_1.Feedback.findByPk(newFeedback.id);
        expect(updatedRecord.companyName).toBe(updatedFeedback.companyName);
    }));
    it('PUT /feedbacks/:id should return 404 if feedback is not found', () => __awaiter(void 0, void 0, void 0, function* () {
        const invalidId = 999;
        const response = yield (0, supertest_1.default)(app)
            .put(`/feedbacks/${invalidId}`)
            .send({
            fromUser: 1,
            companyName: 'Updated Company',
            toUser: 2,
            content: 'Updated feedback content',
        })
            .expect('Content-Type', /json/)
            .expect(constants_1.HttpStatus.NOT_FOUND);
        expect(response.body.message).toBe('Feedback not found');
    }));
    it('DELETE /feedbacks/:id should delete a feedback by ID', () => __awaiter(void 0, void 0, void 0, function* () {
        const newFeedback = yield feedback_model_1.Feedback.create({
            fromUser: 1,
            companyName: 'Test Company',
            toUser: 2,
            content: 'Great work!',
        });
        yield (0, supertest_1.default)(app)
            .delete(`/feedbacks/${newFeedback.id}`)
            .expect(constants_1.HttpStatus.NO_CONTENT);
        const deletedFeedback = yield feedback_model_1.Feedback.findByPk(newFeedback.id);
        expect(deletedFeedback).toBeNull();
    }));
    it('DELETE /feedbacks/:id should return 404 if feedback is not found', () => __awaiter(void 0, void 0, void 0, function* () {
        const invalidId = 999;
        const response = yield (0, supertest_1.default)(app)
            .delete(`/feedbacks/${invalidId}`)
            .expect('Content-Type', /json/)
            .expect(constants_1.HttpStatus.NOT_FOUND);
        expect(response.body.message).toBe('Feedback not found');
    }));
});
//# sourceMappingURL=feedback.test.js.map