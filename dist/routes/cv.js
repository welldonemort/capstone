"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeCvRouter = void 0;
const express_1 = __importDefault(require("express"));
const user_model_1 = require("../models/user.model");
const experience_model_1 = require("../models/experience.model");
const project_model_1 = require("../models/project.model");
const feedback_model_1 = require("../models/feedback.model");
const constants_1 = require("../constants");
const cache_service_1 = require("../services/cache.service");
const cacheService = new cache_service_1.CacheService();
const makeCvRouter = (context) => {
    const router = express_1.default.Router();
    router.get('/user/:userId/cv', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { userId } = req.params;
        const cacheKey = `cv_${userId}`;
        try {
            let cv = yield cacheService.get(cacheKey);
            if (!cv) {
                const user = yield user_model_1.User.findByPk(userId, {
                    attributes: ['id', 'firstName', 'lastName', 'title', 'image', 'summary', 'email'],
                });
                if (!user) {
                    return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'User not found' });
                }
                const experiences = yield experience_model_1.Experience.findAll({
                    where: { userId },
                    attributes: ['userId', 'companyName', 'role', 'startDate', 'endDate', 'description'],
                });
                const projects = yield project_model_1.Project.findAll({
                    where: { userId },
                    attributes: ['id', 'userId', 'image', 'description'],
                });
                const feedbacks = yield feedback_model_1.Feedback.findAll({
                    where: { toUser: userId },
                    attributes: ['id', 'fromUser', 'companyName', 'toUser', 'content'],
                });
                cv = {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    title: user.title,
                    image: user.image,
                    summary: user.summary,
                    email: user.email,
                    experiences,
                    projects,
                    feedbacks,
                };
                yield cacheService.set(cacheKey, cv, 3600);
            }
            res.status(constants_1.HttpStatus.OK).json(cv);
        }
        catch (error) {
            console.error('Error fetching CV:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    return router;
};
exports.makeCvRouter = makeCvRouter;
//# sourceMappingURL=cv.js.map