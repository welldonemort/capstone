"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeAuthRouter = void 0;
const express_1 = __importDefault(require("express"));
const multer_1 = __importDefault(require("multer"));
const path_1 = __importDefault(require("path"));
const passport_1 = __importDefault(require("passport"));
const auth_service_1 = require("../services/auth.service");
const user_model_1 = require("../models/user.model");
const auth_1 = require("../utils/auth");
const roles_1 = require("../middleware/roles");
const express_validator_1 = require("express-validator");
const constants_1 = require("../constants");
const cache_service_1 = require("../services/cache.service");
const PROJECT_ROOT = path_1.default.resolve(__dirname, '../../');
const makeAuthRouter = (context) => {
    const router = express_1.default.Router();
    const authService = new auth_service_1.AuthService();
    const upload = (0, multer_1.default)({ dest: path_1.default.join(PROJECT_ROOT, 'public') });
    router.post('/register', upload.single('image'), [
        (0, express_validator_1.body)('firstName').notEmpty().withMessage('First name is required'),
        (0, express_validator_1.body)('lastName').notEmpty().withMessage('Last name is required'),
        (0, express_validator_1.body)('title').notEmpty().withMessage('Title is required'),
        (0, express_validator_1.body)('summary').notEmpty().withMessage('Summary is required'),
        (0, express_validator_1.body)('email').isEmail().withMessage('Invalid email format'),
        (0, express_validator_1.body)('password').notEmpty().withMessage('Password is required'),
    ], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const errors = (0, express_validator_1.validationResult)(req);
            if (!errors.isEmpty()) {
                return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
            }
            if (!req.file) {
                return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ message: 'No file uploaded' });
            }
            const { email } = req.body;
            const existingUser = yield user_model_1.User.findOne({ where: { email } });
            if (existingUser) {
                return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ message: 'Email already in use' });
            }
            const user = yield authService.registerUser(req.body, req.file);
            res.status(constants_1.HttpStatus.CREATED).json(user);
            yield cache_service_1.cacheService.del('users');
        }
        catch (error) {
            console.error('Error while registering user:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal server error' });
        }
    }));
    router.post('/login', upload.none(), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        passport_1.default.authenticate('local', (err, user, info) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                if (err || !user) {
                    return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ message: 'Invalid email or password' });
                }
                req.login(user, { session: false }, (error) => __awaiter(void 0, void 0, void 0, function* () {
                    if (error) {
                        return next(error);
                    }
                    const token = (0, auth_1.generateToken)(user);
                    return res.json({ user, token });
                }));
            }
            catch (error) {
                return next(error);
            }
        }))(req, res, next);
    }));
    router.get('/example', (0, roles_1.roles)(['Admin']), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        res.json({ message: 'This endpoint is accessible only for Admins' });
    }));
    return router;
};
exports.makeAuthRouter = makeAuthRouter;
//# sourceMappingURL=auth.js.map