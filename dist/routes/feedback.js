"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeFeedbackRouter = void 0;
const express_1 = __importDefault(require("express"));
const feedback_model_1 = require("../models/feedback.model");
const auth_1 = require("../middleware/auth");
const constants_1 = require("../constants");
const express_validator_1 = require("express-validator");
const cache_service_1 = require("../services/cache.service");
const makeFeedbackRouter = (context) => {
    const router = express_1.default.Router();
    router.post('/', auth_1.isAuthenticated, [
        (0, express_validator_1.body)('fromUser').notEmpty().withMessage('From user ID is required'),
        (0, express_validator_1.body)('companyName').notEmpty().withMessage('Company name is required'),
        (0, express_validator_1.body)('toUser').notEmpty().withMessage('To user ID is required'),
        (0, express_validator_1.body)('content').notEmpty().withMessage('Content is required'),
    ], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
        }
        const { fromUser, companyName, toUser, content } = req.body;
        try {
            if (fromUser === toUser) {
                return res
                    .status(constants_1.HttpStatus.BAD_REQUEST)
                    .json({ message: 'Users cannot leave feedback for themselves' });
            }
            const feedback = yield feedback_model_1.Feedback.create({ fromUser, companyName, toUser, content });
            yield cache_service_1.cacheService.del('feedbacks');
            res.status(constants_1.HttpStatus.CREATED).json(feedback);
        }
        catch (error) {
            console.error('Error creating feedback:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.get('/', auth_1.isAuthenticated, auth_1.isAdmin, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { pageSize = 10, page = 1 } = req.query;
        const cacheKey = `feedbacks:${page}:${pageSize}`;
        try {
            let feedbacks = yield cache_service_1.cacheService.get(cacheKey);
            if (!feedbacks) {
                const limit = parseInt(pageSize, 10);
                const offset = (parseInt(page, 10) - 1) * limit;
                feedbacks = yield feedback_model_1.Feedback.findAndCountAll({ limit, offset });
                yield cache_service_1.cacheService.set(cacheKey, feedbacks);
            }
            res.set('X-total-count', feedbacks.count.toString());
            res.status(constants_1.HttpStatus.OK).json(feedbacks.rows);
        }
        catch (error) {
            console.error('Error fetching feedbacks:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { id } = req.params;
        try {
            const feedback = yield feedback_model_1.Feedback.findByPk(id);
            if (!feedback) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'Feedback not found' });
            }
            res.status(constants_1.HttpStatus.OK).json(feedback);
        }
        catch (error) {
            console.error('Error fetching feedback:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.put('/:id', auth_1.isAuthenticated, (0, auth_1.isOwnerOrAdmin)(feedback_model_1.Feedback), [
        (0, express_validator_1.param)('id').isInt().withMessage('Feedback ID must be an integer'),
        (0, express_validator_1.body)('fromUser').notEmpty().withMessage('From user ID is required'),
        (0, express_validator_1.body)('companyName').notEmpty().withMessage('Company name is required'),
        (0, express_validator_1.body)('toUser').notEmpty().withMessage('To user ID is required'),
        (0, express_validator_1.body)('content').notEmpty().withMessage('Content is required'),
    ], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
        }
        const { id } = req.params;
        const { fromUser, companyName, toUser, content } = req.body;
        try {
            const feedback = yield feedback_model_1.Feedback.findByPk(id);
            if (!feedback) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'Feedback not found' });
            }
            feedback.fromUser = fromUser;
            feedback.companyName = companyName;
            feedback.toUser = toUser;
            feedback.content = content;
            yield feedback.save();
            yield cache_service_1.cacheService.del('feedbacks');
            res.status(constants_1.HttpStatus.OK).json(feedback);
        }
        catch (error) {
            console.error('Error updating feedback:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.delete('/:id', auth_1.isAuthenticated, (0, auth_1.isOwnerOrAdmin)(feedback_model_1.Feedback), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { id } = req.params;
        try {
            const feedback = yield feedback_model_1.Feedback.findByPk(id);
            if (!feedback) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'Feedback not found' });
            }
            yield feedback.destroy();
            yield cache_service_1.cacheService.del('feedbacks');
            res.status(constants_1.HttpStatus.NO_CONTENT).send();
        }
        catch (error) {
            console.error('Error deleting feedback:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    return router;
};
exports.makeFeedbackRouter = makeFeedbackRouter;
//# sourceMappingURL=feedback.js.map