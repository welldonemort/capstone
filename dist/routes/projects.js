"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeProjectRouter = void 0;
const express_1 = __importDefault(require("express"));
const multer_1 = __importDefault(require("multer"));
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const project_model_1 = require("../models/project.model");
const auth_1 = require("../middleware/auth");
const constants_1 = require("../constants");
const express_validator_1 = require("express-validator");
const cache_service_1 = require("../services/cache.service");
const storage = multer_1.default.diskStorage({
    destination: (req, file, cb) => {
        const uploadPath = path_1.default.join(__dirname, '../../public/uploads');
        if (!fs_1.default.existsSync(uploadPath)) {
            fs_1.default.mkdirSync(uploadPath, { recursive: true });
        }
        cb(null, uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}-${file.originalname}`);
    },
});
const upload = (0, multer_1.default)({ storage });
const makeProjectRouter = (context) => {
    const router = express_1.default.Router();
    router.post('/', auth_1.isAuthenticated, upload.single('image'), [
        (0, express_validator_1.body)('userId').notEmpty().withMessage('User ID is required'),
        (0, express_validator_1.body)('description').notEmpty().withMessage('Description is required'),
    ], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        var _a;
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
        }
        const { userId, description } = req.body;
        const image = (_a = req.file) === null || _a === void 0 ? void 0 : _a.path;
        try {
            const project = yield project_model_1.Project.create({
                userId,
                image,
                description,
            });
            yield cache_service_1.cacheService.del('projects');
            res.status(constants_1.HttpStatus.CREATED).json(project);
        }
        catch (error) {
            console.error('Error creating project:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.get('/', auth_1.isAuthenticated, auth_1.isAdmin, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { pageSize = 10, page = 1 } = req.query;
        const cacheKey = `projects:${page}:${pageSize}`;
        try {
            let projects = yield cache_service_1.cacheService.get(cacheKey);
            if (!projects) {
                const limit = parseInt(pageSize, 10);
                const offset = (parseInt(page, 10) - 1) * limit;
                projects = yield project_model_1.Project.findAndCountAll({ limit, offset });
                yield cache_service_1.cacheService.set(cacheKey, projects);
            }
            res.set('X-total-count', projects.count.toString());
            res.status(constants_1.HttpStatus.OK).json(projects.rows);
        }
        catch (error) {
            console.error('Error fetching projects:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { id } = req.params;
        try {
            const project = yield project_model_1.Project.findByPk(id);
            if (!project) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'Project not found' });
            }
            res.status(constants_1.HttpStatus.OK).json(project);
        }
        catch (error) {
            console.error('Error fetching project:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.put('/:id', auth_1.isAuthenticated, (0, auth_1.isOwnerOrAdmin)(project_model_1.Project), upload.single('image'), [
        (0, express_validator_1.param)('id').isInt().withMessage('Project ID must be an integer'),
        (0, express_validator_1.body)('userId').notEmpty().withMessage('User ID is required'),
        (0, express_validator_1.body)('description').notEmpty().withMessage('Description is required'),
    ], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        var _b;
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(constants_1.HttpStatus.BAD_REQUEST).json({ errors: errors.array() });
        }
        const { id } = req.params;
        const { userId, description } = req.body;
        const image = (_b = req.file) === null || _b === void 0 ? void 0 : _b.path;
        try {
            const project = yield project_model_1.Project.findByPk(id);
            if (!project) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'Project not found' });
            }
            project.userId = userId;
            project.description = description;
            if (image) {
                project.image = image;
            }
            yield project.save();
            yield cache_service_1.cacheService.del('projects');
            res.status(constants_1.HttpStatus.OK).json(project);
        }
        catch (error) {
            console.error('Error updating project:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    router.delete('/:id', auth_1.isAuthenticated, (0, auth_1.isOwnerOrAdmin)(project_model_1.Project), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { id } = req.params;
        try {
            const project = yield project_model_1.Project.findByPk(id);
            if (!project) {
                return res.status(constants_1.HttpStatus.NOT_FOUND).json({ message: 'Project not found' });
            }
            yield project.destroy();
            yield cache_service_1.cacheService.del('projects');
            res.status(constants_1.HttpStatus.NO_CONTENT).send();
        }
        catch (error) {
            console.error('Error deleting project:', error);
            res.status(constants_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Internal Server Error' });
        }
    }));
    return router;
};
exports.makeProjectRouter = makeProjectRouter;
//# sourceMappingURL=projects.js.map