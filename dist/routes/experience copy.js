
const __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P((resolve) => { resolve(value); }); }
    return new (P || (P = Promise))((resolve, reject) => {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeExperienceRouter = void 0;
const express_1 = __importDefault(require("express"));
const experience_model_1 = require("../models/experience.model");
const auth_1 = require("../middleware/auth");

const makeExperienceRouter = (context) => {
    const router = express_1.default.Router();
    router.post('/', auth_1.isAuthenticated, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { userId, companyName, role, startDate, endDate, description } = req.body;
        try {
            if (!userId || !companyName || !role || !startDate || !description) {
                return res.status(400).json({ message: 'Validation failed: missing fields' });
            }
            const experience = yield experience_model_1.Experience.create({
                userId,
                companyName,
                role,
                startDate,
                endDate,
                description,
            });
            res.status(201).json(experience);
        }
        catch (error) {
            console.error('Error creating experience:', error);
            res.status(500).json({ message: 'Internal Server Error' });
        }
    }));
    router.get('/', auth_1.isAuthenticated, auth_1.isAdmin, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { pageSize = 10, page = 1 } = req.query;
        try {
            const limit = parseInt(pageSize, 10);
            const offset = (parseInt(page, 10) - 1) * limit;
            const experiences = yield experience_model_1.Experience.findAndCountAll({ limit, offset });
            res.set('X-total-count', experiences.count.toString());
            res.status(200).json(experiences.rows);
        }
        catch (error) {
            console.error('Error fetching experiences:', error);
            res.status(500).json({ message: 'Internal Server Error' });
        }
    }));
    router.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { id } = req.params;
        try {
            const experience = yield experience_model_1.Experience.findByPk(id);
            if (!experience) {
                return res.status(404).json({ message: 'Experience not found' });
            }
            res.status(200).json(experience);
        }
        catch (error) {
            console.error('Error fetching experience:', error);
            res.status(500).json({ message: 'Internal Server Error' });
        }
    }));
    router.put('/:id', auth_1.isAuthenticated, (0, auth_1.isOwnerOrAdmin)(experience_model_1.Experience), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { id } = req.params;
        const { userId, companyName, role, startDate, endDate, description } = req.body;
        try {
            const experience = yield experience_model_1.Experience.findByPk(id);
            if (!experience) {
                return res.status(404).json({ message: 'Experience not found' });
            }
            experience.userId = userId;
            experience.companyName = companyName;
            experience.role = role;
            experience.startDate = startDate;
            experience.endDate = endDate;
            experience.description = description;
            yield experience.save();
            res.status(200).json(experience);
        }
        catch (error) {
            console.error('Error updating experience:', error);
            res.status(500).json({ message: 'Internal Server Error' });
        }
    }));
    router.delete('/:id', auth_1.isAuthenticated, (0, auth_1.isOwnerOrAdmin)(experience_model_1.Experience), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { id } = req.params;
        try {
            const experience = yield experience_model_1.Experience.findByPk(id);
            if (!experience) {
                return res.status(404).json({ message: 'Experience not found' });
            }
            yield experience.destroy();
            res.status(204).send();
        }
        catch (error) {
            console.error('Error deleting experience:', error);
            res.status(500).json({ message: 'Internal Server Error' });
        }
    }));
    return router;
};
exports.makeExperienceRouter = makeExperienceRouter;