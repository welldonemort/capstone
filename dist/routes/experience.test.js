"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const experience_model_1 = require("../models/experience.model");
const index_1 = require("../index");
let app;
beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
    app = yield index_1.appPromise;
}));
describe('Experience Router', () => {
    it('should create a new experience', () => __awaiter(void 0, void 0, void 0, function* () {
        const newExperience = {
            userId: 1,
            companyName: 'Test Company',
            role: 'Developer',
            startDate: '2024-06-01',
            endDate: '2024-06-30',
            description: 'Worked on backend development',
        };
        const response = yield (0, supertest_1.default)(app)
            .post('/')
            .send(newExperience)
            .expect('Content-Type', /json/)
            .expect(201);
        expect(response.body).toMatchObject(newExperience);
        const createdExperience = yield experience_model_1.Experience.findByPk(response.body.id);
        expect(createdExperience).toBeDefined();
        expect(createdExperience.companyName).toBe(newExperience.companyName);
    }));
    it('should fetch all experiences', () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app)
            .get('/')
            .expect('Content-Type', /json/)
            .expect(200);
        expect(Array.isArray(response.body)).toBe(true);
        expect(response.header['x-total-count']).toBeDefined();
        expect(parseInt(response.header['x-total-count'], 10)).toBeGreaterThan(0);
    }));
    it('should fetch a single experience by ID', () => __awaiter(void 0, void 0, void 0, function* () {
        const newExperience = yield experience_model_1.Experience.create({
            userId: 1,
            companyName: 'Test Company',
            role: 'Developer',
            startDate: new Date('2024-06-01'),
            endDate: new Date('2024-06-30'),
            description: 'Worked on backend development',
        });
        const response = yield (0, supertest_1.default)(app)
            .get(`/${newExperience.id}`)
            .expect('Content-Type', /json/)
            .expect(200);
        expect(response.body.id).toBe(newExperience.id);
    }));
    it('should update an experience by ID', () => __awaiter(void 0, void 0, void 0, function* () {
        const newExperience = yield experience_model_1.Experience.create({
            userId: 1,
            companyName: 'Test Company',
            role: 'Developer',
            startDate: new Date('2024-06-01'),
            endDate: new Date('2024-06-30'),
            description: 'Worked on backend development',
        });
        const updatedExperience = {
            userId: 1,
            companyName: 'Updated Company',
            role: 'Senior Developer',
            startDate: '2024-07-01',
            endDate: '2024-07-31',
            description: 'Worked on frontend development',
        };
        const response = yield (0, supertest_1.default)(app)
            .put(`/${newExperience.id}`)
            .send(updatedExperience)
            .expect('Content-Type', /json/)
            .expect(200);
        expect(response.body).toMatchObject(updatedExperience);
        const updatedRecord = yield experience_model_1.Experience.findByPk(newExperience.id);
        expect(updatedRecord.companyName).toBe(updatedExperience.companyName);
    }));
});
//# sourceMappingURL=experience.test.js.map