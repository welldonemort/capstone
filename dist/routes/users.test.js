"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const sequelize_1 = require("sequelize");
const user_model_1 = require("../models/user.model");
const cache_service_1 = require("../services/cache.service");
const index_1 = require("../index");
let app;
beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
}));
describe('User Routes', () => __awaiter(void 0, void 0, void 0, function* () {
    let sequelize;
    beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
        app = yield index_1.appPromise;
        sequelize = new sequelize_1.Sequelize({
            dialect: 'sqlite',
            storage: ':memory:',
        });
        user_model_1.User.initialize(sequelize);
        yield sequelize.sync({ force: true });
    }));
    afterAll(() => __awaiter(void 0, void 0, void 0, function* () {
        yield sequelize.close();
    }));
    afterEach(() => __awaiter(void 0, void 0, void 0, function* () {
        yield user_model_1.User.destroy({ where: {} });
        yield cache_service_1.cacheService.del('users');
    }));
    it('should create a new user', () => __awaiter(void 0, void 0, void 0, function* () {
        const newUser = {
            firstName: 'John',
            lastName: 'Doe',
            email: 'john.doe@example.com',
            password: 'password',
            role: 'user',
        };
        const response = yield (0, supertest_1.default)(app).post('/users').send(newUser);
        expect(response.status).toBe(201);
        expect(response.body).toHaveProperty('id');
        expect(response.body.firstName).toBe(newUser.firstName);
        expect(response.body.lastName).toBe(newUser.lastName);
        expect(response.body.email).toBe(newUser.email);
        expect(response.body.role).toBe(newUser.role);
    }));
    it('should fetch all users', () => __awaiter(void 0, void 0, void 0, function* () {
        yield user_model_1.User.bulkCreate([
            { firstName: 'John', lastName: 'Doe', email: 'john.doe@example.com', password: 'password', role: 'User' },
            { firstName: 'Jane', lastName: 'Smith', email: 'jane.smith@example.com', password: 'password', role: 'Admin' },
        ]);
        const response = yield (0, supertest_1.default)(app).get('/users');
        expect(response.status).toBe(200);
        expect(response.body.length).toBe(2);
        expect(response.headers['x-total-count']).toBeDefined();
    }));
    it('should fetch a single user by ID', () => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield user_model_1.User.create({
            firstName: 'John',
            lastName: 'Doe',
            email: 'john.doe@example.com',
            password: 'password',
            role: 'User',
        });
        const response = yield (0, supertest_1.default)(app).get(`/users/${user.id}`);
        expect(response.status).toBe(200);
        expect(response.body.id).toBe(user.id);
        expect(response.body.firstName).toBe(user.firstName);
        expect(response.body.lastName).toBe(user.lastName);
        expect(response.body.email).toBe(user.email);
        expect(response.body.role).toBe(user.role);
    }));
    it('should update a user', () => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield user_model_1.User.create({
            firstName: 'John',
            lastName: 'Doe',
            email: 'john.doe@example.com',
            password: 'password',
            role: 'User',
        });
        const updatedUser = {
            firstName: 'Updated',
            lastName: 'User',
            email: 'updated.user@example.com',
            role: 'admin',
        };
        const response = yield (0, supertest_1.default)(app).put(`/users/${user.id}`).send(updatedUser);
        expect(response.status).toBe(200);
        expect(response.body.id).toBe(user.id);
        expect(response.body.firstName).toBe(updatedUser.firstName);
        expect(response.body.lastName).toBe(updatedUser.lastName);
        expect(response.body.email).toBe(updatedUser.email);
        expect(response.body.role).toBe(updatedUser.role);
    }));
    it('should delete a user', () => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield user_model_1.User.create({
            firstName: 'John',
            lastName: 'Doe',
            email: 'john.doe@example.com',
            password: 'password',
            role: 'User',
        });
        const response = yield (0, supertest_1.default)(app).delete(`/users/${user.id}`);
        expect(response.status).toBe(204);
        const deletedUser = yield user_model_1.User.findByPk(user.id);
        expect(deletedUser).toBeNull();
    }));
}));
//# sourceMappingURL=users.test.js.map