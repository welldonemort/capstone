"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const sequelize_1 = require("sequelize");
const project_model_1 = require("../models/project.model");
const cache_service_1 = require("../services/cache.service");
const index_1 = require("../index");
let app;
describe('Project Routes', () => __awaiter(void 0, void 0, void 0, function* () {
    let sequelize;
    app = yield index_1.appPromise;
    beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
        sequelize = new sequelize_1.Sequelize({
            dialect: 'sqlite',
            storage: ':memory:',
        });
        project_model_1.Project.defineSchema(sequelize);
        yield sequelize.sync({ force: true });
    }));
    afterAll(() => __awaiter(void 0, void 0, void 0, function* () {
        yield sequelize.close();
    }));
    afterEach(() => __awaiter(void 0, void 0, void 0, function* () {
        yield project_model_1.Project.destroy({ where: {} });
        yield cache_service_1.cacheService.del('projects');
    }));
    it('should create a new project', () => __awaiter(void 0, void 0, void 0, function* () {
        const newProject = {
            userId: 1,
            description: 'Test project description',
        };
        const response = yield (0, supertest_1.default)(app).post('/projects').send(newProject);
        expect(response.status).toBe(201);
        expect(response.body).toHaveProperty('id');
        expect(response.body.userId).toBe(newProject.userId);
        expect(response.body.description).toBe(newProject.description);
    }));
    it('should fetch all projects', () => __awaiter(void 0, void 0, void 0, function* () {
        yield project_model_1.Project.bulkCreate([
            { userId: 1, description: 'Project 1' },
            { userId: 1, description: 'Project 2' },
        ]);
        const response = yield (0, supertest_1.default)(app).get('/projects');
        expect(response.status).toBe(200);
        expect(response.body.length).toBe(2);
        expect(response.headers['x-total-count']).toBeDefined();
    }));
    it('should fetch a single project by ID', () => __awaiter(void 0, void 0, void 0, function* () {
        const project = yield project_model_1.Project.create({ userId: 1, description: 'Single project' });
        const response = yield (0, supertest_1.default)(app).get(`/projects/${project.id}`);
        expect(response.status).toBe(200);
        expect(response.body.id).toBe(project.id);
        expect(response.body.userId).toBe(project.userId);
        expect(response.body.description).toBe(project.description);
    }));
    it('should update a project', () => __awaiter(void 0, void 0, void 0, function* () {
        const project = yield project_model_1.Project.create({ userId: 1, description: 'Old description' });
        const updatedProject = {
            userId: 2,
            description: 'Updated description',
        };
        const response = yield (0, supertest_1.default)(app).put(`/projects/${project.id}`).send(updatedProject);
        expect(response.status).toBe(200);
        expect(response.body.id).toBe(project.id);
        expect(response.body.userId).toBe(updatedProject.userId);
        expect(response.body.description).toBe(updatedProject.description);
    }));
    it('should delete a project', () => __awaiter(void 0, void 0, void 0, function* () {
        const project = yield project_model_1.Project.create({ userId: 1, description: 'To be deleted' });
        const response = yield (0, supertest_1.default)(app).delete(`/projects/${project.id}`);
        expect(response.status).toBe(204);
        const deletedProject = yield project_model_1.Project.findByPk(project.id);
        expect(deletedProject).toBeNull();
    }));
}));
//# sourceMappingURL=projects.test.js.map