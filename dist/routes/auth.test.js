"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const index_1 = require("../index");
let app;
beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
    app = yield index_1.appPromise;
}));
describe('Auth Routes', () => {
    it('should register a new user', () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app)
            .post('/api/auth/register')
            .send({
            firstName: 'John',
            lastName: 'Doe',
            title: 'Developer',
            summary: 'Experienced developer',
            email: 'john.doe@example.com',
            password: 'password123',
        });
        expect(response.status).toBe(201);
        expect(response.body).toHaveProperty('firstName', 'John');
    }));
    it('should login with valid credentials', () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app)
            .post('/api/auth/login')
            .send({
            email: 'john.doe@example.com',
            password: 'password123',
        });
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('token');
    }));
    it('should reject login with invalid credentials', () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(app)
            .post('/api/auth/login')
            .send({
            email: 'john.doe@example.com',
            password: 'wrongpassword',
        });
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty('message', 'Invalid email or password');
    }));
});
//# sourceMappingURL=auth.test.js.map