module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  transform: {
    '^.+\\.ts$': 'ts-jest',
  },
  moduleNameMapper: {
    '^node:(.*)$': '$1',
  },
  modulePathIgnorePatterns: ['<rootDir>/dist/'],
};
